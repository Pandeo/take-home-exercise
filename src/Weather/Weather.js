import React, {Component} from 'react';
import './Weather.css';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

class Weather extends Component {
    
render() {
  return (
      <div>
        <Grid container spacing={3}>
        <Grid item xs={12} width="75%">
        <TextField
          error
          id="celsius"
          label="Celsius"
          defaultValue="79"
          variant="filled"
          style={{width:'75%'}}
        />
        </Grid>
        <Grid item xs={12} width="75%">
        <TextField
          error
          id="fahrenheit"
          label="Fahrenheit"
          defaultValue="79"
          variant="filled"
          style={{width:'75%'}}
        />
        </Grid>
        </Grid>
        </div>
  );
}
}

export default Weather;
