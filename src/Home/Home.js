import React, { useState } from 'react';
import './Home.css';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import history from './../history';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const api = {
    key: "ff9f895b2e884d6680530135202710",
    base: "http://api.weatherapi.com/v1/current.json"
}

function Home() {
  const classes = useStyles();

  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState({});

  const search = evt => {
    if (evt.key === "Enter") {
      fetch(`${api.base}weather?q=${query}&units=metric&APPID=${api.key}`)
        .then(res => res.json())
        .then(result => {
          setWeather(result);
          setQuery('');
          console.log(result);
        });
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <Grid container spacing={3}>
        <Grid item xs={12} width="75%">
        <TextField
          error
          id="api-key"
          label="Your API key"
          defaultValue="ff9f895b2e884d6680530135202710"
          variant="filled"
          style={{width:'75%'}}
        />
        </Grid>
        <Grid item xs={12} width="75%">
        <FormControl className={classes.formControl} error style={{width:'75%'}}>
        <InputLabel id="select-city-name-label">City Name</InputLabel>
        <Select
          labelId="select-city-name-label"
          id="select-city-name"
          onChange={e => setQuery(e.target.value)}
          value={query}
        >
          <MenuItem value="">
          </MenuItem>
          <MenuItem value="Kuala Lumpur" >Kuala Lumpur</MenuItem>
          <MenuItem value="Singapore" >Singapore</MenuItem>
        </Select>
      </FormControl>
      </Grid>
      <Grid item xs={12} >
        <Button width="75%" variant="contained" color="secondary" style={{width:'75%'}} onClick={search}> {/*  () => history.push('/Weather') */}
          Submit
        </Button>
        </Grid>
        </Grid>
      </header>
    </div>
  );
}

export default Home;